# Visual Designer and Marketing Manager
![GNOME_Profile](/uploads/a52d83959f40aa5c46f5c55f048e27ef/GNOME_Profile.png)

## About Me
- Name: Daniel Galleguillos Cruz.
- Country: Calama - Chile. -22.4624°, -68.9272°
- Contact: dgalleguillos@gnome.org
- Contact: daniel@gnome.cl
- Mastodon: [dnlgalleguillos](https://floss.social/@dnlgalleguillos)
- Mastodon: [GNOME Latam](https://floss.social/@GNOMELatam)
- Telegram: https://t.me/DanielGalleguillos
- Artificial Intelligence: https://www.youtube.com/@danielgpl
- Twitter: [dnlgalleguillos](https://twitter.com/dnlgalleguillos)
- Twitter: [GNOME Chile](https://twitter.com/diagnome)
- Twitter: [GNOME Latam](https://twitter.com/GNOMELatam)
- GitLab: [dnlgalleguillos](https://gitlab.gnome.org/dnlgalleguillos)
- Flickr: [Pictures and A.I](https://www.flickr.com/photos/daniel_gc/)
- Element: [@daniel_gc:matrix.org](https://matrix.org/)

## GNOME Graphic Designs and Marketing Manachments

### 2025 Blender Geometry Nodes and Terminal Codes.
- https://gitlab.gnome.org/dnlgalleguillos/Blender-3D-Geometry-Nodes

### 2024 Part of the organization of GNOME Latam, a day to celebrate and expand our GNOME Community in Latin America.
- https://gitlab.gnome.org/Teams/Engagement/Social-Media-and-News/-/issues/354
- https://gitlab.gnome.org/Teams/Engagement/Social-Media-and-News/-/issues/355
- https://events.gnome.org/event/249/

### 2024 openSUSE Asia Summit 2024 Logo Competition.
- [openSUSE Asia Summit 2024 Logos](https://gitlab.gnome.org/dnlgalleguillos/profile/-/issues/2)

### 2024 Converts .WebM files to .OGG using ffmpeg in Python.
- https://gitlab.gnome.org/dnlgalleguillos/webm_to_ogg

### 2023 GNOME Asia How to collaborate with GNOME as a Visual Designer.
- https://gitlab.gnome.org/dnlgalleguillos/talks
- https://www.youtube.com/watch?v=Bg92CjLtK2o&t=2515s

### 2023 Part of the organization of GNOME Latam, a day to celebrate and expand our GNOME Community in Latin America.
- https://events.gnome.org/event/136/
- https://gitlab.gnome.org/Teams/Engagement/Events/gnome-latam/gnome-latam-2023

### 2023 In coordination with Caroline Henriksen, GNOME Latam has two official accounts on social networks.
- https://twitter.com/GNOMELatam
- https://floss.social/@GNOMELatam

### 2022 GNOME Asia How to collaborate with GNOME as a Visual Designer.
- https://gitlab.gnome.org/dnlgalleguillos/talks
- https://www.youtube.com/watch?v=qWLQHFCMQFM&t=2485s From:16:28:00

### 2022 I Submitted a proposal for Kinetic Kudu 22.10 Ubuntu Wallpaper Competition.
- Shader Editor Created with Blender 3.4.0 Alpha. - CC BY-SA 4.0
- https://discourse.ubuntu.com/t/kinetic-kudu-22-10-wallpaper-competition/30319/34

### 2022 GUADEC 2022 How to be a Graphic Designer for GNOME Engagement Team.
- https://gitlab.gnome.org/dnlgalleguillos/talks
- https://www.youtube.com/watch?v=fWViBfR14D4 From:4:10:00

### 2022 I created some T-Shirt for Engagement Team, there are some ideas for GNOME Store.
- https://gitlab.gnome.org/Teams/Engagement/General/-/issues/152

### 2021 GNOME Asia Design with GNOME Engagement Team.
- https://gitlab.gnome.org/dnlgalleguillos/talks
- https://www.youtube.com/watch?v=59IW2MOSpR4&t=12998s From:2:00:00

### 2021 I created a logo for GNOME.Asia Summit 2021 proposal contest.
- [GNOME.Asia Summit 2021 proposal](https://gitlab.gnome.org/dnlgalleguillos/profile/-/issues/3)

### 2021 I'm stating to create a video about GNOME Handibox.
- What is Handiibox
- How can I use Handibox
- https://gitlab.gnome.org/Teams/Engagement/Social-Media-and-News/-/issues/247 

### 2021 I created all the videos for GNOME Latam 2021 with:
- Blender 3D, Inkscape and Creative Commons Music
- https://gitlab.gnome.org/Teams/Engagement/Social-Media-and-News/-/issues/247

### 2021 I give my talk in promotion about GNOME Latam 2021 in Spanish in a popular tech post “RockIT Chile”.
- https://www.youtube.com/watch?v=xFt9P0eQoFI&ab_channel=RockITChile

### 2021 I give my talk about GNOME Art for GNOME Latam with Fernanda Morales "Diseño en GNOME con Engagement Team".
- https://www.youtube.com/watch?v=LvBh9G6bK4I&ab_channel=GNOME

### 2021 Part of the organization of GNOME Latam, a day to celebrate and expand our GNOME Community in Latin America.
- https://events.gnome.org/event/82/
- https://gitlab.gnome.org/Teams/Engagement/Social-Media-and-News/-/issues/211

### 2021 I'm very happy to start working as a Designer for Handibox is an accessibility tool that will allow Human-Computer interaction with people who have some degree of motor disability.
- https://gitlab.gnome.org/fabioduran/handibox

### 2020 I communicate with Caroline Henriksen our Brand Manager I sent my Portfolio and now I'm part of GNOME Engagement Team.
- https://gitlab.gnome.org/Teams/Engagement

### 2020 I submitted my wallpapers for Debian Release Bullseye.
- https://wiki.debian.org/DebianArt/Themes/SoftDebian
- https://wiki.debian.org/DebianArt/Themes/Debian3D

### 2020 I create a Graphic to prevent Coronavirus GNOME - Do The Five COVID19 Created with Blender3D and Inkscape.
- https://www.flickr.com/photos/daniel_gc/49701163271/

### 2019-20 I created and Published GNOME Stickers and Wallpapers for COVID-19.
- https://gitlab.gnome.org/Teams/Engagement/Events/GUADEC/guadec-2020/-/issues/56
- https://www.flickr.com/photos/daniel_gc/49701163271/in/album-72157602474582554/

### 2019 I Designed the promotion for UbuCon Latinoamérica 2019 with Inkscape.
- https://wiki.gnome.org/DanielGalleguillos/dia_gnome_banner

### 2018 I Designed with Inkscape for The Annual GNOME Bugzilla Statistics for 2018 - Andre Klapper for his support of this design.
- https://wiki.gnome.org/DanielGalleguillos/Bugzilla
- https://wiki.gnome.org/DanielGalleguillos/GNOME_Bugsquad_Render

### 2016 I created a GPSAmi Logo, a GUI app to read data from GPS devices, created with Hubert Figuière.
- https://github.com/hfiguiere/gpsami

### 2011 - Now, I designed and still design some Safety Procedures with Blender 3D for safety presentations and Safety Graphics with Inkscape into a copper mine company.
- https://www.flickr.com/photos/daniel_gc/collections/72157719103941768/

### 2010 The GNOME Marketing I created a GNOME T-Shirt for GNOME Store.
- https://wiki.gnome.org/DanielGalleguillos/poleras_GNOME

### 2010 I was Into the GNOME Marketing Team and I designed the GNOME Annual Report 2009 with Paul Cutler, Stormy Peters and all the GNOME Team. The Annual Report was created with Inkscape and GIMP.
- https://www.gnome.org/wp-content/uploads/2011/11/gnome-annual-report-2009.pdf
- https://wiki.gnome.org/DanielGalleguillos/GNOME_Annual_Report_Mockup
- https://paulcutler.org/blog/2010/12/2009-gnome-annual-report-is-out/

### 2009 Working as a safety supervisor in a copper mine I created all the Safety Procedures under Blender 3D for capacitation. I was in the Chilean Tesis Final with my "Safety 3D Capacitation for workers".
- https://diario.uach.cl/premian-tesis-de-alto-impacto-tecnolgicola-domtica-de-la-uach-destaca-a-nivel-nacional/
- http://danielgpl.blogspot.com/2006/11/linux-en-tesis-digital.html

### 2009 I'm still active until now to create GNOME Hackergotchi or any logo.
- https://wiki.gnome.org/action/show/Design/ArtRequests

### 2009 I won the Ubuntu Free Culture Showcase. Build To Heaven (Photo/Graphics)
- https://wiki.ubuntu.com/UbuntuFreeCultureShowcase/PreviousWinners

### 2009 I designed a Big GNOME Graphic Poster for Global Talks.
- https://wiki.gnome.org/DanielGalleguillos/GNOME_Gigantografia
- https://2013.guadec.org/wp-content/themes/wordcamp-base/images/guadec2012.png

### 2009 I designed Ubuntu Wallpapers for Ubuntu Chile. with: Blender3D - Inkscape - GIMP.
- https://wiki.ubuntu.com/ChileanTeam/GrupoArte/Wallpapers

### 2008 I won the GNOME Wallpaper Contest Release 2.24
- http://danielgpl.blogspot.com/2008/08/new-backgrounds-for-gnome-224.html

### 2007 I met the GNOME Chile team into a Encuentro Linux and I was welcomed to this great community. - From 2007 still now I'm a GNOME Chile Designer and I created all the Banners and Stickers for Chilean Talks. 
- https://twitter.com/diagnome
- https://gitlab.com/fabianarias/noticias.gnome.cl

### 2006 I was teaching Linux/GNOME in INACAP Center for Technical Professionals.
- https://portales.inacap.cl/admision/index

### 2005 We held the first Seminar for migration to Linux in Calama (Calix: Calama + Linux) my talk was about Open Source Graphics Tools.
- https://www.flickr.com/photos/danielgpl/150534600/in/dateposted/

### 2005 I’d started to collaborate with GNOME Art art.gnome.org creating:
- Wallpapers
- GDM. (GNOME Display Manager)
- https://wiki.gnome.org/AndreasNilsson Told me about the GNOME Art and how to collaborate with design.
- https://www.flickr.com/photos/daniel_gc/albums/72157602474582554
